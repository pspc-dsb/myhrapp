// Import the LitElement base class and html helper function, using ES6 modules from a module CDN
import {LitElement, html} from 'https://jspm.dev/lit-element@2';

// Extend the LitElement base class
export class LinkList extends LitElement {

  // The constructor lets us define properties
  constructor() {
    super();
  }

  // The render function will return HTML
  render(){
    // The "html`...`;" construction is a JavaScript "template literal"
    // that allows you to mix in JavaScript to display data in html
    return html`
      <div class="list-group">
        ${this.elementChildren}
      </div>
    `;
  }

  // Override rendering function to avoid using shadowdom, so that the rendered html can use css classes / js already defined on the page.
  createRenderRoot() {
    return this;
  }

  // Since not using shadowdom (to get page css/js inherited), using a custom load handler that finds all the element's children, and puts them in this.elementChildren.
  // this.elementChildren can be used in the render function to output the children like a <slot></slot> element if we were using shadowdom.
  connectedCallback(){
    super.connectedCallback();
    this.elementChildren = Array.from(this.childNodes);
    this.elementChildren.forEach(child => {
      // find any children with text of the form [link](url)
      if (child.nodeName === '#text' && child.textContent.match(/\[.*\]\(.*\)/)) {
        const matches = child.textContent.match(/\[.*\]\(.*\)/g);
        let span;
        // for each match, convert it to an html 'a' element with appropriate Bootstrap classes.
        matches.forEach((match, i) => {
          const text = match.substring(match.indexOf('[')+1, match.indexOf(']'));
          const link = match.substring(match.indexOf('(')+1, match.indexOf(')'));
          const a = document.createElement('a');
          a.href = link;
          a.innerText = text;
          a.classList.add(...['list-group-item', 'list-group-item-action', 'text-primary']);
          if (i === 0) {
            span = document.createElement('span');
            child.replaceWith(span);
          }
          span.appendChild(a);
        });
      } else if (child.nodeName === 'A') {
        child.classList.add(...['list-group-item', 'list-group-item-action', 'text-primary']);
      }
    });
  }

}

// Register the new element for use in the browser
customElements.define('html-link-list', LinkList);
