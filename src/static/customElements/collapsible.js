// Import the LitElement base class and html helper function, using ES6 modules from a module CDN
import {LitElement, html} from 'https://jspm.dev/lit-element@2';

let count = 1;

// Extend the LitElement base class
export class Collapsible extends LitElement {

  // The constructor lets us define properties
  constructor() {
    super();
    this.label = '';
    this.open = false;
    this.id = count++;
    this.parentId = '';
  }

  // Overrides LitElement getter for properties. Gets HTML element's corresponding attribute as a JS variable.
  static get properties() {
    return {
      label: { type: String },
      open: { type: Boolean },
      id: {attribute: false} // prevent automatically adding this property as an attribute in dom
    }
  }

  // The render function will return HTML
  render(){
    // The "html`...`;" construction is a JavaScript "template literal"
    // that allows you to mix in JavaScript to display data in html
    return html`
      <div class="card collapsible">
        <div class="card-header p-0" id="card-header-${this.id}">
          <h5 class="mb-0"><button class="list-group-item list-group-item-action text-primary ${this.open ? '' : 'collapsed'}" data-toggle="collapse" data-target="#collapse-${this.id}" aria-expanded="${this.open?'true':'false'}" aria-controls="collapse-${this.id}">${this.label}</button></h5>
        </div>
        <div id="collapse-${this.id}" class="collapse${this.open ? ' show' : ''}" aria-labelledby="card-header-${this.id}" data-parent="${this.parentId}">
          <div class="card-body p-1 ml-3">
            ${this.elementChildren}
          </div>
        </div>
      </div>
    `;
  }

  // Override rendering function to avoid using shadowdom, so that the rendered html can use css classes / js already defined on the page.
  createRenderRoot() {
    return this;
  }

  // Since not using shadowdom (to get page css/js inherited), using a custom load handler that finds all the element's children, and puts them in this.elementChildren.
  // this.elementChildren can be used in the render function to output the children like a <slot></slot> element if we were using shadowdom.
  connectedCallback(){
    super.connectedCallback();
    this.elementChildren = Array.from(this.childNodes);
    if (this.parentNode?.nodeName === 'HTML-ACCORDION' && this.parentNode?.getAttribute('id')?.startsWith('accordion')) {
      this.parentId = '#' + this.parentNode.getAttribute('id');
    }
  }

}

// Register the new element for use in the browser
customElements.define('html-collapsible', Collapsible);
