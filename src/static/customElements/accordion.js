// Import the LitElement base class and html helper function, using ES6 modules from a module CDN
import {LitElement, html} from 'https://jspm.dev/lit-element@2';

let count = 1;

// Extend the LitElement base class
export class Accordion extends LitElement {

  // The constructor lets us define properties
  constructor() {
    super();
    this.label = '';
    this.open = false;
    this.id = 'accordion' + count++;
  }

  // Overrides LitElement getter for properties. Gets HTML element's corresponding attribute as a JS variable.
  static get properties() {
    return {
      label: { type: String },
      open: { type: Boolean }
    }
  }

  // The render function will return HTML
  render(){
    // The "html`...`;" construction is a JavaScript "template literal"
    // that allows you to mix in JavaScript to display data in html
    return html`
      ${this.elementChildren}
    `;
  }

  // Override rendering function to avoid using shadowdom, so that the rendered html can use css classes / js already defined on the page.
  createRenderRoot() {
    return this;
  }

  // Since not using shadowdom (to get page css/js inherited), using a custom load handler that finds all the element's children, and puts them in this.elementChildren.
  // this.elementChildren can be used in the render function to output the children like a <slot></slot> element if we were using shadowdom.
  connectedCallback(){
    super.connectedCallback();
    this.elementChildren = Array.from(this.childNodes);
  }

}

// Register the new element for use in the browser
customElements.define('html-accordion', Accordion);
