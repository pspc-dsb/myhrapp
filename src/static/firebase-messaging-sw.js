importScripts('/js/firebase.min.js');

// Overriding firebase.messaging's push notification handler because it only sets title, description, and links to the base url.
// They don't let you customize notification messages, just data messages, but the notification console can't send data messages.
self.originalAddEventListener = self.addEventListener;
// This prevents firebase from registering a push listener and the notification click handler
self.addEventListener = function (type, listener) {
    type === "push" || type === "notificationclick" || self.originalAddEventListener.apply(this, arguments);
};

const messaging = firebase.messaging();

// now that firebase is loaded, allow registering event listeners again
self.addEventListener = self.originalAddEventListener;

// register our own push event listener
self.addEventListener('push', function(event) {
  // Check if we have permission to show notifications from the user
  if (!(self.Notification && self.Notification.permission === 'granted')) {
    return;
  }
  let data = {};
  if (event.data) {
    // the data from firebase notification console is in json format, conforming to this template:
    // https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#Notification
    // which uses the minimal set of values (title, body, image) that all platforms support.
    data = event.data.json();
    // This registers the listener that shows the notification to the user
    return self.registration.showNotification(data.notification.title, {
      body: data.notification.body,
      icon: '/images/covid.png', // TODO: change icon to app icon once it's made
      data: data.data
    });
  }
});

self.addEventListener('notificationclick', function(event) {
  event.notification.close(); // Android needs explicit close.
  // This checks whether the desired client is already open, and focuses it if it is.
  event.waitUntil(self.clients.matchAll({
    type: "window",
    includeUncontrolled: true
  }).then(function(clientList) {
    for (let i = 0; i < clientList.length; i++) {
      let client = clientList[i];
      if (client.url.match(self.location.host) && 'focus' in client) {
        // if a link was provided in the notification, and its link contains the current page's host...
        if (event.notification.data.link && event.notification.data.link.match(self.location.host)) {
          // send the data to the page to use
          client.postMessage({
            title: event.notification.title || '',
            body: event.notification.body || '',
            icon: event.notification.data.icon || '',
            tag: event.notification.data.tag || '',
            link: event.notification.data.link || ''
          });
          // and focus the page
          return client.focus();
        }
      }
    }
    // otherwise, open the url in the client
    if (self.clients.openWindow) {
      return self.clients.openWindow(event.notification.data.link || self.location.protocol + "//" + self.location.host);
    }
  }));
});
