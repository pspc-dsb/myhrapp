import { Accordion } from './customElements/accordion.js';
import { Collapsible } from './customElements/collapsible.js';
import { LinkList } from './customElements/link-list.js';

const customTags = { Accordion, Collapsible, LinkList };
export { customTags };