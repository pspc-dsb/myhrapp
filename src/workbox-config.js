module.exports = {
  "globDirectory": "/home/node/app/.fb/public",
  "globPatterns": [
    "**/*.{html,css,jpg,xml,svg,png,js,json,eot,ttf,woff,woff2}"
  ],
  "swDest": "/home/node/app/src/static/sw.js",
  "inlineWorkboxRuntime": true,
  "mode": "production",
  "cleanupOutdatedCaches": true/*,
  // commented out because these files aren't served with cors headers anymore, so using local copies (src/assets/js/firebase/) instead
  "additionalManifestEntries": [
    {"url": "https://www.gstatic.com/firebasejs/7.21.1/firebase-app.js", "revision": null},
    {"url": "https://www.gstatic.com/firebasejs/7.21.1/firebase-analytics.js", "revision": null},
    {"url": "https://www.gstatic.com/firebasejs/7.21.1/firebase-auth.js", "revision": null},
    {"url": "https://www.gstatic.com/firebasejs/7.21.1/firebase-messaging.js", "revision": null},
    {"url": "https://www.gstatic.com/firebasejs/7.21.1/firebase-firestore.js", "revision": null}
  ]*/
};