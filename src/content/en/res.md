---
title: Resources
path: res
description: ""
date: 2020-10-30T14:33:09.744Z
---
<html-accordion>
  <html-collapsible label="Workplace harassment and violence" open>
    <html-link-list>
      [New Wrokplace Harassment and Violence Regulations for Defence Team Public Servants (Bill C-65)](#)
      [How do I submit a harassment or violence complaint?](#)
      [Bill C-65 overview](#)
      [Training](#)
      [The Workplace Harassment and Violence Prevention Centre of Expertise](#)
      [Other support services and resources](#)
      [Questions and answers](#)
    </html-link-list>
  </html-collapsible>
  <html-collapsible label="Pay, Benefits & Leave">
    <html-collapsible label="Pay">
      <html-link-list>
        [Pay Basics](#)
        [View and Understand Your Pay](#)
        [Understand Our Paycheques](#)
        [Employee Roles in Pay](#)
        [Overtime](#)
        [Reporting your Pay Issue](#)
        [Emergency Salary Advance](#)
        [Visit MyGCPay](#)
      </html-link-list>
    </html-collapsible>
  </html-collapsible>
</html-accordion>