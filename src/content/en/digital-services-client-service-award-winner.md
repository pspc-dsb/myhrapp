---
title: Digital Services Client Service Award winner
path: client-service-award-winners-2020
date: 2020-12-11T19:45:29.564Z
---
The [PSPC Client Service Award](https://masource-mysource.spac-pspc.gc.ca/eng/services/rh-hr/carriere-career/reconnaissance-recognition/Pages/prix-service-client-award.aspx) recognizes the contributions of employees and teams demonstrating service excellence.

This year's recipient of the Digital Services Branch’s (DSB) Client Service Award is the Digital Workplace Programme team.

COVID-19 upended our jobs, and most employees have made the abrupt shift to working from home. This new reality brought along an increased need for new communication and collaboration tools to allow employees to continue to function in their virtual workplace. The Digital Workplace Programme team promptly responded to this need by accelerating the deployment of Microsoft Teams in a fraction of the time. The accelerated project started in late April and a pilot was launched in July. By mid-August, Microsoft Teams was deployed across the department.

Rolling out a new software to over 19,000 employees during a pandemic is no easy feat. Add in the pressures of teleworking while taking care of family and personal commitments and it’s easy to understand the challenges the team faced. They went above and beyond, working in collaboration with many DSB sectors, to ensure that all aspects of the project were adequately addressed. From comprehensive project planning to configuration, security and service management, the team demonstrated service excellence to ensure that employees had the tools to be productive in their new virtual workplace, all in record time!

Congratulations to the Digital Workplace Programme team!

![A collage of 9 photos of PSPC employees holding a certificate.](http://intranet.tpsgc-pwgsc.gc.ca/dlc-ink/av-fs/images/2020-12-04-00.png)