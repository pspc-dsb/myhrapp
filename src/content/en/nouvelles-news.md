---
title: News and announcements
path: nouvelles-news
description: ""
date: 2020-11-30T15:02:50.045Z
---
<div class="list-group">
  <a href="../dates-limites-deadlines" class="list-group-item list-group-item-action text-primary">Holiday season deadlines to submit and approve timesheets</a>
</div>
