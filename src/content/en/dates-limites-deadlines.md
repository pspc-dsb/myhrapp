---
title: Holiday season deadlines to submit and approve timesheets
path: dates-limites-deadlines
description: ""
date: 2020-11-30T15:02:50.045Z
---
With the holiday season fast approaching, there are changes to the normal timelines to submit and approve timesheets. To receive basic pay, employees and managers must meet the deadlines listed below.

This applies to:

- Employees who work on an on-call basis; or
- Indeterminate and Term on an approved progressive return to work program (Disability/Rehabilitation).

---
## Employees reporting to section 34 managers

- Submit hours in Phoenix timesheet for their section 34 managers

| Time period | Deadline |
|---|---|
| 26 November 2020 - 9 December 2020 | By 7 December 2020 (for hours worked up to 9 December)<br/><br/>By 10 December 2020 (for hours worked on 8 & 9 December 2020) |
| 10 December 2020 - 23 December 2020 | By 24 December 2020 |
| 24 December 2020 - 6 January 2021 | By January 10 2021&ast; | 

**&ast;Deadline may be adjusted based on Phoenix pay processing schedule for 24 December 2020 - 6 January 2021 (pay period 2)**

---
## Section 34 manager

- Approve in Phoenix Pay System

| Time period | Deadline |
|---|---|
| 26 November 2020 - 9 December 2020 | By 8 December 2020 (for hours worked up to 9 December)<br/><br/>By 11 December 2020 (for hours worked on 8 & 9 December 2020) |
| 10 December 2020 - 23 December 2020 | By 29 December 2020 |
| 24 December 2020 - 6 January 2021 | By January 11 2021&ast; |

**&ast;Deadline may be adjusted based on Phoenix pay processing schedule for 24 December 2020 - 6 January 2021 (pay period 2)**