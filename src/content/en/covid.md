---
title: Covid-19
path: covid
description: ""
date: 2020-11-27T14:44:25.843Z
---
<div class="container">
  <div class="row">
    <div class="col-sm">
</div>
    <div class="col-sm">
      <p><a class="btn btn-primary btn-block" href="https://www.canada.ca/en/public-health/services/diseases/2019-novel-coronavirus-infection.html" role="button">GoC Updates</a></p>
    </div>
    <div class="col-sm">
</div>
  </div>
  <div class="row">
    <div class="col-sm">
</div>
    <div class="col-sm">
      <p><a class="btn btn-primary btn-block" href="#" role="button">Contacts &amp; Assistance</a></p>
    </div>
    <div class="col-sm">
</div>
  </div>
</div>
<br/>
<nav>
  <div class="nav nav-pills flex-column flex-sm-row" id="nav-tab" role="tablist">
    <a class="nav-item nav-link flex-sm-fill active" id="nav-faq-tab" data-toggle="tab" href="#nav-faq" role="tab" aria-controls="nav-faq" aria-selected="true">FAQ</a>
    <a class="nav-item nav-link flex-sm-fill" id="nav-well-being-tab" data-toggle="tab" href="#nav-well-being" role="tab" aria-controls="nav-well-being" aria-selected="false">Well-being</a>
    <a class="nav-item nav-link flex-sm-fill" id="nav-mgr-tab" data-toggle="tab" href="#nav-mgr" role="tab" aria-controls="nav-mgr" aria-selected="false">MGR</a>
    <a class="nav-item nav-link flex-sm-fill" id="nav-tools-tab" data-toggle="tab" href="#nav-tools" role="tab" aria-controls="nav-tools" aria-selected="false">Tools</a>
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-faq" role="tabpanel" aria-labelledby="nav-faq-tab">
    <div class="list-group">
      <a href="#" class="list-group-item list-group-item-action text-primary">Frequently Asked Questions - COVID-19 (As of November 26, 2020)</a>
      <a href="#" class="list-group-item list-group-item-action text-primary">Ask anything: COVID-19</a>
    </div>
  </div>
  <div class="tab-pane fade" id="nav-well-being" role="tabpanel" aria-labelledby="nav-well-being-tab">
    <a href="#" class="list-group-item list-group-item-action text-primary">Update: "Other Leave with Pay" Code 699</a>
  </div>
  <div class="tab-pane fade" id="nav-mgr" role="tabpanel" aria-labelledby="nav-mgr-tab">
    <a href="#" class="list-group-item list-group-item-action text-primary">Managers: UPDATE - Important Information on SLE for Managers Staffing Positions</a>
  </div>
  <div class="tab-pane fade" id="nav-tools" role="tabpanel" aria-labelledby="nav-tools-tab">
    <a href="#" class="list-group-item list-group-item-action text-primary">Remote Work Reimbursement Support Message</a>
    <a href="#" class="list-group-item list-group-item-action text-primary">Virtual Recognition Toolkit</a>
  </div>
</div>
