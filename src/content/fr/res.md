---
title: Ressources
path: res
description: ""
date: 2020-10-30T14:33:18.530Z
---
{{<accordion "Paie, avantages sociaux et congés">}}
  {{< accordion "Paie" >}}
    <div class="list-group">
      <a href="#" class="list-group-item list-group-item-action text-primary">Principes de base de la rémunération</a>
      <a href="#" class="list-group-item list-group-item-action text-primary">Consulter et comprendre votre paye</a>
      <a href="#" class="list-group-item list-group-item-action text-primary">Comprendre le relevé de paie</a>
      <a href="#" class="list-group-item list-group-item-action text-primary">Rôles des employés</a>
      <a href="#" class="list-group-item list-group-item-action text-primary">Heures supplémentaires</a>
      <a href="#" class="list-group-item list-group-item-action text-primary">Problèmes de paye</a>
      <a href="#" class="list-group-item list-group-item-action text-primary">Demande d'avance de salaire en cas d'urgence</a>
      <a href="#" class="list-group-item list-group-item-action text-primary">Consultez MaPayeGC</a>
    </div>
  {{</accordion>}}
{{</accordion>}}