---
title: Dates limites de soumission et d'approbation des feuilles de temps pour la période de Fêtes
path: dates-limites-deadlines
description: ""
date: 2020-11-30T15:02:50.045Z
---
Comme la période des Fêtes approche à grands pas, des changements ont été apportés aux délais normaux de soumission et d'approbation des feuilles de temps. Les employés et les gestionnaires doivent respecter les échéances indiquées ci-dessous pour que les employés reçoivent leur rémunération de base.

Ceci s'applique aux :

- employés qui travaillent sur appel ; ou
- employés nommés pour une période indéterminée ou déterminée dans le cadre d'un programme approuvé de retour progressif au travail (invalidité/réadaptation).

---
## Employés relevant d'un gestionnaire détenant des pouvoirs en vertu de l'article 34

- Soumettre les feuilles de temps à leur gestionnaire autorisé de l'article 34 par l'entremise de Phénix.

| Périod de temps | Date limite |
|---|---|
| Du 26 novembre 2020 au 9 décembre 2020 | Au plus tard le 7 décembre 2020 (pour les heures travaillées jusq'au 9 décembre)<br/><br/>Au plus tard le 10 décembre 2020 (pour les heures travaillées le 8 et 9 décembre 2020) |
| Du 10 décembre 2020 au 23 décembre 2020 | Au plus tard le 24 décembre 2020 |
| Du 24 décembre 2020 au 6 janvier 2021 | Au plus tard le 10 janvier 2021&ast; |

**&ast;La date limite peut être ajustée en fonction du calendrier de traitement de la paye Phénix du 24 décembre 2020 au 6 janvier 2021 (période de paye 2)**

---
## Gestionnaire détenant des pouvoirs en vertu de l'article 34

- Approuver dans le système de paye Phénix

| Périod de temps | Date limite |
|---|---|
| Du 26 novembre 2020 au 9 décembre 2020 | Au plus tard le 8 décembre 2020 (pour les heures travaillées jusq'au 9 décembre)<br/><br/>Au plus tard le 11 décembre 2020 (pour les heures travaillées le 8 et 9 décembre 2020) |
| Du 10 décembre 2020 au 23 décembre 2020 | Au plus tard le 29 décembre 2020 |
| Du 24 décembre 2020 au 6 janvier 2021 | Au plus tard le 11 janvier 2021&ast; |

**&ast;La date limite peut être ajustée en fonction du calendrier de traitement de la paye Phénix du 24 décembre 2020 au 6 janvier 2021 (période de paye 2)**