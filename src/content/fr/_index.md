---
title: Accueil
description: ""
date: 2020-09-24T17:23:52.000Z
---

<a href="covid" class="rainbow-row-1"><img src="../images/covid.png" alt="covid" /> Covid 19</a>
<a href="nouvelles-news" class="rainbow-row-2"><img src="../images/news.png" alt="nouvelles" /> Nouvelles et communiqués</a>
<a href="res" class="rainbow-row-3"><img src="../images/resources.png" alt="ressources" /> Ressources</a>
<a href="#" class="rainbow-row-4"><img src="../images/working.png" alt="travailler" /> Travailler aux SPAC</a>
<a href="#" class="rainbow-row-5"><img src="../images/toolbox.png" alt="outils" /> Outils</a>
<a href="#" class="rainbow-row-6"><img src="../images/videos.png" alt="vidéos" /> Vidéos</a>
<a href="#" class="rainbow-row-7"><img src="../images/contacts.png" alt="contacts" /> Mes contacts</a>
