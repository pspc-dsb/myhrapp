---
title: Covid-19
path: covid
description: ""
date: 2020-11-27T14:44:25.843Z
---
<div class="container">
  <div class="row">
    <div class="col-sm">
</div>
    <div class="col-sm">
      <p><a class="btn btn-primary btn-block" href="https://www.canada.ca/fr/sante-publique/services/maladies/2019-nouveau-coronavirus.html" role="button">GdC : mises à jour</a></p>
    </div>
    <div class="col-sm">
</div>
  </div>
  <div class="row">
    <div class="col-sm">
</div>
    <div class="col-sm">
      <p><a class="btn btn-primary btn-block" href="#" role="button">Contacts et assistance</a></p>
    </div>
    <div class="col-sm">
</div>
  </div>
</div>
<br/>
<nav>
  <div class="nav nav-pills flex-column flex-sm-row" id="nav-tab" role="tablist">
    <a class="nav-item nav-link flex-sm-fill active" id="nav-faq-tab" data-toggle="tab" href="#nav-faq" role="tab" aria-controls="nav-faq" aria-selected="true">FAQ</a>
    <a class="nav-item nav-link flex-sm-fill" id="nav-well-being-tab" data-toggle="tab" href="#nav-well-being" role="tab" aria-controls="nav-well-being" aria-selected="false">Bien-être</a>
    <a class="nav-item nav-link flex-sm-fill" id="nav-mgr-tab" data-toggle="tab" href="#nav-mgr" role="tab" aria-controls="nav-mgr" aria-selected="false">GEST</a>
    <a class="nav-item nav-link flex-sm-fill" id="nav-tools-tab" data-toggle="tab" href="#nav-tools" role="tab" aria-controls="nav-tools" aria-selected="false">Outils</a>
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active" id="nav-faq" role="tabpanel" aria-labelledby="nav-faq-tab">
    <div class="list-group">
      <a href="#" class="list-group-item list-group-item-action text-primary">Foire aux questions - COVID-19 (nouveau coronavirus 2019) En date du 26 novembre 2020</a>
      <a href="#" class="list-group-item list-group-item-action text-primary">Demandez n'importe quoi &emdash; COVID-19</a>
    </div>
  </div>
  <div class="tab-pane fade" id="nav-well-being" role="tabpanel" aria-labelledby="nav-well-being-tab">
    <a href="#" class="list-group-item list-group-item-action text-primary">Mise à jour : « Autres congés payés » (code 699)</a>
  </div>
  <div class="tab-pane fade" id="nav-mgr" role="tabpanel" aria-labelledby="nav-mgr-tab">
    <a href="#" class="list-group-item list-group-item-action text-primary">Les gestionnaires : Mise à jour - Information importante sur ÉLS pour les gestionnaires qui ont des postes à pourvoir</a>
  </div>
  <div class="tab-pane fade" id="nav-tools" role="tabpanel" aria-labelledby="nav-tools-tab">
    <a href="#" class="list-group-item list-group-item-action text-primary">Message concernant l'aide au remboursement pour le travail à distance</a>
    <a href="#" class="list-group-item list-group-item-action text-primary">Trousse virtuelle de reconnaissance</a>
  </div>
</div>
