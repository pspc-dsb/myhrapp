$(document).ready(() => {
  const containers = Array.from(document.querySelectorAll('div[class*=WidgetPreviewContainer]'));
  if (containers.length) {
    const date = containers.filter(element => {return element.innerHTML.includes(' GMT-')});
    if (date.length) {
      date[0].hidden = true;
    }
  }
});
