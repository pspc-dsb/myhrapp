window.install_banner = document.querySelector("#install-banner");

if (install_banner) {

  // Handle showing the custom installation prompt
  window.addEventListener('beforeinstallprompt', (e) => {
    // Prevent the mini-infobar from appearing on mobile
    e.preventDefault();
    // Stash the event so it can be triggered later.
    window.deferredPrompt = e;
    // Update UI notify the user they can install the PWA
    if (localStorage.promptInstall !== "false") {
      window.install_banner.classList.remove("fade", "invisible");
    }
  });

  // Handle the install button click
  window.install_banner.addEventListener("click", (e) => {
    if (!window.deferredPrompt) {
      return; // The deferred propmt isn't available.
    }
    if (e.target.classList.contains("close") || e.target.parentElement.classList.contains("close")) {
      // bootstrap handles closing it, but we should remember they don't want it installed.
      localStorage.promptInstall = "false";
      return;
    }
    // Show the prompt
    window.deferredPrompt.prompt();
    window.deferredPrompt.userChoice.then(result => {
      // Hide it
      window.install_banner.classList.add("fade", "invisible");
      localStorage.promptInstall = "false";
    });
  });
}

window.install_banner_ios = document.querySelector("#install-banner-ios");

if (install_banner_ios) {
  // Detects if device is on iOS
  const isIos = () => {
    const userAgent = window.navigator.userAgent.toLowerCase();
    return /iphone|ipad|ipod/.test( userAgent );
  }
  // Detects if device is in standalone mode
  const isInStandaloneMode = () => ('standalone' in window.navigator) && (window.navigator.standalone);
  // Checks if should display install popup notification:
  if (isIos() && !isInStandaloneMode()) {
    // Update UI notify the user they can install the PWA
    if (localStorage.promptInstall !== "false") {
      window.install_banner_ios.classList.remove("fade", "invisible");
    }
  }
  window.install_banner_ios.addEventListener("click", (e) => {
    if (e.target.classList.contains("close") || e.target.parentElement.classList.contains("close")) {
      // bootstrap handles closing it, but we should remember they don't want it installed.
      localStorage.promptInstall = "false";
      return;
    }
  });
}
