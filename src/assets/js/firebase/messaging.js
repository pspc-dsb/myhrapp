if (window.Notification && window.Notification.requestPermission) {
  const messaging = firebase.messaging();
  const auth = firebase.auth();

  // Configure Web credentials
  // The method usePublicVapidKey allows FCM to use the VAPID key credential when sending message requests to different 
  // push services. Using the key you generated or imported according to the instructions in Configure Web Credentials 
  // with FCM, add it in your code after the messaging object is retrieved: 
  messaging.usePublicVapidKey("BKuXM-g2nclYusafao63jPx0WFih5Rh1SdKJPxXiHpZhMxRSTBvA-RWrDPuP9JnSkgNdCngxDJL4iepxLmUPzrs");

  // Access the registration token
  // When you need to retrieve the current registration token for an app instance, call getToken. If notification permission 
  // has not been granted, this method will ask the user for notification permissions. Otherwise, it returns a token or 
  // rejects the promise due to an error.
  // The messaging service requires a firebase-messaging-sw.js file. Unless you already have a firebase-messaging-sw.js file, 
  // create an empty file with that name and place it in the root of your domain before retrieving a token. You can add 
  // meaningful content to the file later in the client setup process.
  // Get Instance ID token. Initially this makes a network call, once retrieved
  // subsequent calls to getToken will return from cache.
  messaging.getToken().then((currentToken) => {
    if (currentToken) {
      localStorage.token = currentToken;
    }
  }).catch((err) => {
    console.log('An error occurred while retrieving token. User likely blocked notifications.');
  });

  // Handle incoming messages.
  navigator.serviceWorker.addEventListener('message', async (event) => {
    auth.signInAnonymously().catch(error => {
      if (error.code === 'auth/operation-not-allowed') {
        console.error('You must enable anonymous auth in the Firebase Console.');
      } else {
        console.error(error);
      }
    }).then(async () => {
      // iOS support
      localStorage[`last_${lang}_message`] = JSON.stringify(event.data);
      // Save the message in firestore for iOS devices to pick up later
      const db = firebase.firestore();
      // First check if it's already been stored by another client
      const result = await db.collection(`last_message`).doc(lang).get();
      if (result && result.data()) {
        const lastMessage = result.data();
        const lastSeenMessage = localStorage[`last_${lang}_message`];
        // If not store it for all clients
        if (messageIsDifferent(lastSeenMessage, lastMessage)) {
          await db.collection(`last_message`).doc(lang).set(event.data).catch();
        }
      } else {
        await db.collection(`last_message`).doc(lang).set(event.data).catch();
      }
      // end of iOS support
      location.href = event.data.link;
    });
  });
}

// iOS support
if (!window.Notification || !window.Notification.requestPermission) {
  const db = firebase.firestore();
  const auth = firebase.auth();
  window.addEventListener('load', () => {
    auth.signInAnonymously().catch(error => {
      if (error.code === 'auth/operation-not-allowed') {
        console.error('You must enable anonymous auth in the Firebase Console.');
      } else {
        console.error(error);
      }
    }).then(async () => {
      const lastSeenMessage = localStorage[`last_${lang}_message`];
      const result = await db.collection(`last_message`).doc(lang).get();
      if (result && result.data()) {
        const lastMessage = result.data();
        if (messageIsDifferent(lastSeenMessage, lastMessage)) {
          localStorage[`last_${lang}_message`] = JSON.stringify(lastMessage);
          // show the new message
          showMessageModal(lastMessage);
        }
      }
    });
  });
}

const messageIsDifferent = (lastSeenMessage, lastMessage) => {
  if (typeof lastSeenMessage === 'undefined') return true;
  if (JSON.parse(lastSeenMessage).title != lastMessage.title)  return true;
  if (JSON.parse(lastSeenMessage).body != lastMessage.body)  return true;
  if (JSON.parse(lastSeenMessage).link != lastMessage.link)  return true;
  if (JSON.parse(lastSeenMessage).tag != lastMessage.tag)  return true;
  if (JSON.parse(lastSeenMessage).icon != lastMessage.icon)  return true;
  return false;
}

const showMessageModal = (lastMessage) => {
  const modalHTML = `
    <div class="modal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">${lastMessage.title}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" ${lastMessage.link ? 'onclick="location.href=\'' + lastMessage.link + '\'"' : ''}>
            <p>${lastMessage.body}</p>
          </div>
        </div>
      </div>
    </div>`;
  const div = document.createElement('div');
  div.innerHTML = modalHTML;
  document.body.appendChild(div.children[0]);
  $('.modal').modal();
};

// end of iOS support
