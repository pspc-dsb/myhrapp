# MyHRApp - PSPC

**MyHRApp** is a reusable open-source web/mobile app that uses a <abbr title="What You See Is What You Get">WYSIWYG</abbr> editor to edit app content, and an automated <abbr title="Continuous Integration/Continous Deployment">CI/CD</abbr> pipeline to deliver new content quickly to end users. The app content is non-confidential Human Resources information for government of Canada employees in <abbr title="Public Services and Government Services Canada">PSPC</abbr>, but can easily be repurposed for other departments by forking the GitLab repo, and customizing department-specific info and the Firebase configuration files.
>[![](https://img.shields.io/badge/license-MIT-blue)](https://gitlab.com/pspc-dsb/myhrapp/-/blob/master/LICENSE)<br/>(Open source initiative)

# Table of contents
* [Application architecture](#application-architecture)
* [Technology stack](#technology-stack)
* [Content Management System usage](#content-management-system-usage)
* [Development](#development)
  * [Prerequisites](#prerequisites)
  * [Docker](#docker)
  * [Git](#git)
  * [Source code](#source-code)
  * [Usage](#usage)
    * [Run the Hugo Dev server](#run-the-hugo-dev-server)
    * [Hugo build](#hugo-build)
    * [Firebase setup](#firebase-setup)
    * [Firebase scripts](#firebase-scripts)
    * [Netlify CMS setup](#netlify-cms-setup)
  * [Contributing](#contributing)

# Application architecture

![diagram](http://www.plantuml.com/plantuml/svg/fLR_Rjis4FvVJy4P6s07azbum30OXb3_IBm0QneaBeeY3GPAF4kSAL8WgNhUa7Vfi-J9HagY72KER63_n2Jlk-_utHrFUQyDKQRCUS1XRP2HIB4qCuWo1ObXKWJ13qmalAI09gOi25M_u4JckHJH0SjQ2XEIBMYP0yvbp3WSXHaISp17OPmKHXP7OHJqfiPHM3C1wD8gYOc66oiIPSOKUeon8jjXoanMngK6bLW246PeTU7DQ4l4Hk9runOoQzLXmObmQS8vqGOKzllsEgNYdL3OR-PLieR5IU2VHkz-bzemaUBcztJ7EPSbjRyVG6kIEWg_EaQob3BbZc8g2DyPbcZShchNG6-OA8WYER9f9IKtbrAOQv83Mf5CvkGrO0L_cobdgSX169JOFw2UeKXYfBepEmweATrBuumacrJ9Kj2vv5AXRSRCSv1hK2BinISJMqfEuYYQIKL1rV2b0X27uGKhW3C19pluPkWwN5xTNiocTnT_VhoTNYyl3hkw_kgwpNW9GM1a2YO3XVuDKAqNzIQQ_GFZyz-YixCBokpHsTa4kxEeXuX6K9qTX2_W6t1Pm0iF2j-2no1eh-DzHk-Y-tl_5khaLSrVw289gOvnliiKAGgCbipyGUA7XvPrdsmNe5ux3StjK1X6g3-l7zG0ZL5l7qXbkY19w89aQhY5UEX249epWT7JzwVldx_SNNt-ilmg-jTWE5llqFp3tU3XeHSW9yLpKnZOWrUYpdyzKLI8VGDeDB_2ymMwQVOTdHvKA_MxIYj2_Ld9E1sWaCgjSDTQWJO3PsgMUyi22YvtplOOe3OtBS1JrJOAhmp7A-WcbEGmz6qwJAi7NQblNpggvaD7eSVN2lqELHARyTDVn-S_lrd4i2IcBQHtiVMo7f-cgzwWmpDwc-SDZXibQKNIJgC-yIjhFYq1XP9_GMAw0N7BW_VaTT05qLaiYQeZjoEqd_lL_umif65hbX33f40tfSxGzREJ0vTu37mBLG_05WID4IcDpAr7yj8uiiQSY9BmmrtpIODgMy2FfyuGIjoK6gySlOmcFpPVyGckpgF6l9ISWhYLr2BgBtEB-2eQp5mgiUPo2ygDF0lqt-ONN0kYDX-LlQZ3rFyDF2TZqJqHEpS0M0AsVLazszpD0_UYWcRoJC8mTDa9N19iCcf4nzfMtP-63jTMDd0UbUiUs1htbEz1q3Bd_m40)
(Click the diagram to open a version in another tab where the links are clickable)<br/>
([edit diagram](http://www.plantuml.com/plantuml/uml/fLR_Rjis4FvVJy4P6s07azbum30OXb3_IBm0QneaBeeY3GPAF4kSAL8WgNhUa7Vfi-J9HagY72KER63_n2Jlk-_utHrFUQyDKQRCUS1XRP2HIB4qCuWo1ObXKWJ13qmalAI09gOi25M_u4JckHJH0SjQ2XEIBMYP0yvbp3WSXHaISp17OPmKHXP7OHJqfiPHM3C1wD8gYOc66oiIPSOKUeon8jjXoanMngK6bLW246PeTU7DQ4l4Hk9runOoQzLXmObmQS8vqGOKzllsEgNYdL3OR-PLieR5IU2VHkz-bzemaUBcztJ7EPSbjRyVG6kIEWg_EaQob3BbZc8g2DyPbcZShchNG6-OA8WYER9f9IKtbrAOQv83Mf5CvkGrO0L_cobdgSX169JOFw2UeKXYfBepEmweATrBuumacrJ9Kj2vv5AXRSRCSv1hK2BinISJMqfEuYYQIKL1rV2b0X27uGKhW3C19pluPkWwN5xTNiocTnT_VhoTNYyl3hkw_kgwpNW9GM1a2YO3XVuDKAqNzIQQ_GFZyz-YixCBokpHsTa4kxEeXuX6K9qTX2_W6t1Pm0iF2j-2no1eh-DzHk-Y-tl_5khaLSrVw289gOvnliiKAGgCbipyGUA7XvPrdsmNe5ux3StjK1X6g3-l7zG0ZL5l7qXbkY19w89aQhY5UEX249epWT7JzwVldx_SNNt-ilmg-jTWE5llqFp3tU3XeHSW9yLpKnZOWrUYpdyzKLI8VGDeDB_2ymMwQVOTdHvKA_MxIYj2_Ld9E1sWaCgjSDTQWJO3PsgMUyi22YvtplOOe3OtBS1JrJOAhmp7A-WcbEGmz6qwJAi7NQblNpggvaD7eSVN2lqELHARyTDVn-S_lrd4i2IcBQHtiVMo7f-cgzwWmpDwc-SDZXibQKNIJgC-yIjhFYq1XP9_GMAw0N7BW_VaTT05qLaiYQeZjoEqd_lL_umif65hbX33f40tfSxGzREJ0vTu37mBLG_05WID4IcDpAr7yj8uiiQSY9BmmrtpIODgMy2FfyuGIjoK6gySlOmcFpPVyGckpgF6l9ISWhYLr2BgBtEB-2eQp5mgiUPo2ygDF0lqt-ONN0kYDX-LlQZ3rFyDF2TZqJqHEpS0M0AsVLazszpD0_UYWcRoJC8mTDa9N19iCcf4nzfMtP-63jTMDd0UbUiUs1htbEz1q3Bd_m40))

--------------------------------------------------------------------------------

# Technology stack

<p><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/Netlify_CMS_logo.svg/800px-Netlify_CMS_logo.svg.png" height=40 alt="Netlify CMS"/></p>

>[![](https://img.shields.io/badge/license-MIT-blue)](https://github.com/netlify/netlify-cms)<br/>(Open source initiative)

[Netlify CMS](https://www.netlifycms.org/) is a portable Content Management System that can edit markdown files in a Git repository. It handles the git commands behind the scenes to synchronize changes made to files in the <abbr title="What You See Is What You Get">WYSIWYG</abbr> editor to the version-controlled code repository.

| Editor-firendly user interface | Intuitive workflow for content teams |
|---|---|
|![user interface](https://d33wubrfki0l68.cloudfront.net/31eb1152e28887205b52d7b45164f286d070dc37/63313/img/cms/netlify-cms1.png) | ![workflow](https://d33wubrfki0l68.cloudfront.net/643f3998ba2b15df30f439a928c3928b9bb3b92f/a4300/img/cms/netlify-cms2.png) |
| The web-based app includes rich-text editing, real-time preview, and drag-and-drop media uploads. | Editors can easily manage content from draft to review to publish across any number of custom content types. |

--------------------------------------------------------------------------------

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/Logo_of_Hugo_the_static_website_generator.svg/800px-Logo_of_Hugo_the_static_website_generator.svg.png" height=40 alt="Hugo"/> 

>[![](https://img.shields.io/badge/license-Apache_2-blue)](https://github.com/gohugoio/hugo)<br/>(Open source initiative)

[Hugo](https://gohugo.io/) is one of the most popular open-source static site generators. With its amazing speed and flexibility, Hugo makes building websites fun again. Hugo uses extensible markdown to build static HTML files.

--------------------------------------------------------------------------------

<img src="https://developers.google.com/web/tools/workbox/images/pwa.svg" height=40 alt="PWA"/>

Portable Web Apps are web apps that can install natively to mobile devices. They support many of the native app capabilities, and have the reach and discoverability (search engines) of web apps. Support is evolving, and is especially limited on Apple iOS devices. For more information, see [What are PWAs?](https://web.dev/what-are-pwas/)

--------------------------------------------------------------------------------

<img src="https://user-images.githubusercontent.com/110953/28352645-7a8a66d8-6c0c-11e7-83af-752609e7e072.png" height=40 alt="Workbox"/>

>[![](https://img.shields.io/badge/license-MIT-blue)](https://github.com/GoogleChrome/workbox)<br/>(Open source initiative)

[Google Workbox](https://developers.google.com/web/tools/workbox) is a developer library for adding offline support to web apps. It can be used to generate instructions for web apps to cache content for offline usage, in a background "service worker" process that doesn't block the UI.

--------------------------------------------------------------------------------

<img src="https://about.gitlab.com/images/logos/wm_web.svg" height=40 alt="GitLab"/>

>[![](https://img.shields.io/badge/license-MIT-blue)](https://gitlab.com/gitlab-org/gitlab/-/blob/master/LICENSE)<br/>(Open source initiative)

GitLab.com is a complete DevOps platform. A single interface to manage, plan, create, verify, build, package, secure, release, configure, monitor, and protect apps.

--------------------------------------------------------------------------------

<img src="https://firebase.google.com/downloads/brand-guidelines/PNG/logo-standard.png" height=40 alt="Firebase"/><br/>

[Google Firebase](https://firebase.google.com/) is a free or paid cloud platform, offering free static hosting, storage, authentication, push notifications, analytics, and more.

--------------------------------------------------------------------------------

# Content Management System usage

When you navigate to [/admin/](https://rhspac-pspchr.web.app/admin/) you'll be prompted to log in with your GitLab credentials, and once authenticated you'll be able to create new content or edit existing content. The landing page is a list of pages you can edit. Just click on a page name (or the "New page" button) to open the editor.

The editor is split into several panels by default. On the left, you can edit metadata and content. And a preview will be shown on the right.

You can toggle the content language to edit French content, by selecting "**Writing in FR**" from the first drop-down.

Alternatively, you can edit English and French side-by-side by toggling the editor layout mode with the button at the top-right, just under your user icon. It looks like this: <img src="https://raw.githubusercontent.com/netlify/netlify-cms/master/packages/netlify-cms-ui-default/src/Icon/images/page.svg" height="20" alt="page icon"/>.

In the **Body** section, you can edit content in either "**Rich Text**" (WYSIWYG) mode, or "**Markdown**" mode. Markdown is like HTML, but a little simpler. Either mode also supports mixing in HTML, for more complicated content that you can't achieve with markdown. For a quick overview of markdown, try [this tutorial](https://commonmark.org/help/tutorial/) or refer to [this reference](https://commonmark.org/help/).

The "**Rich Text**" toolbar has common formatting buttons:  
<img src="https://raw.githubusercontent.com/netlify/netlify-cms/master/packages/netlify-cms-ui-default/src/Icon/images/bold.svg" height="30" alt="bold icon" title="bold"/>
<img src="https://raw.githubusercontent.com/netlify/netlify-cms/master/packages/netlify-cms-ui-default/src/Icon/images/italic.svg" height="30" alt="italic icon" title="italic"/>
<img src="https://raw.githubusercontent.com/netlify/netlify-cms/master/packages/netlify-cms-ui-default/src/Icon/images/code.svg" height="30" alt="code icon" title="code"/>
<img src="https://raw.githubusercontent.com/netlify/netlify-cms/master/packages/netlify-cms-ui-default/src/Icon/images/link.svg" height="30" alt="link icon" title="link"/>
<img src="https://raw.githubusercontent.com/netlify/netlify-cms/master/packages/netlify-cms-ui-default/src/Icon/images/h-options.svg" height="30" alt="headings icon" title="headings"/>
<img src="https://raw.githubusercontent.com/netlify/netlify-cms/master/packages/netlify-cms-ui-default/src/Icon/images/quote.svg" height="30" alt="quote icon" title="quote"/>
<img src="https://raw.githubusercontent.com/netlify/netlify-cms/master/packages/netlify-cms-ui-default/src/Icon/images/list-bulleted.svg" height="30" alt="bulleted list icon" title="bulleted list"/>
<img src="https://raw.githubusercontent.com/netlify/netlify-cms/master/packages/netlify-cms-ui-default/src/Icon/images/list-numbered.svg" height="30" alt="numbered list icon" title="numbered list"/>
<img src="https://raw.githubusercontent.com/netlify/netlify-cms/master/packages/netlify-cms-ui-default/src/Icon/images/add-with.svg" height="30" alt="add component icon" title="add component"/>

When you're ready to save your page, you can press the teal **Publish** button at the top-right. Publishing will save your content files (files for French and English) in the GitLab repository, and trigger a pipeline that will incorporate that content into the web app, automatically deploying to the [UAT server](https://rhspac-pspchr-uat.web.app/).

After reviewing it on the UAT server, you can manually trigger a Prod deployment by running the **Prod** deploy job in [GitLab's Pipelines page](https://gitlab.com/pspc-dsb/myhrapp/-/pipelines).

--------------------------------------------------------------------------------

# Development

## Prerequisites

You'll need to install Docker, Git, and clone this repo before you'll be able to contribute. Also, you'll need a GitLab account with developer permissions granted to this repo, and a Firebase account granted the Editor role.

### Docker

If you're using Windows for this project, [WSL2](https://docs.microsoft.com/en-us/windows/wsl/install-win10) with [Docker integration](https://docs.docker.com/docker-for-windows/wsl/) is highly recommended.

| <a href="https://docs.docker.com/docker-for-windows/install/"><h3><img src="https://simpleicons.org/icons/windows.svg" height="14"/> **Docker Desktop for Windows**</h3></a> | <a href="https://docs.docker.com/docker-for-mac/install/"><h3><img src="https://simpleicons.org/icons/apple.svg" height="16"/> **Docker Desktop for Mac**</h3></a> | <a href="https://docs.docker.com/engine/install/"><h3><img src="https://simpleicons.org/icons/linux.svg" height="14"/> **Docker for Linux**</h3></a> |
|---|---|---|
| A native Windows application which delivers all Docker tools to your Windows computer. | A native application using the macOS sandbox security model which delivers all Docker tools to your Mac. | Install Docker on a computer which already has a Linux distribution installed. |

Check out [this tutorial](https://docs.docker.com/get-started/) for an introduction to Docker.

### Git

| <a href="https://git-scm.com/download/win"><h3><img src="https://simpleicons.org/icons/windows.svg" height="14"/> **Git for Windows**</h3></a> | <a href="https://git-scm.com/download/mac"><h3><img src="https://simpleicons.org/icons/apple.svg" height="16"/> **Git for Mac**</h3></a> | <a href="https://docs.docker.com/engine/install/"><h3><img src="https://simpleicons.org/icons/linux.svg" height="14"/> **Git for Linux**</h3></a> |
|---|---|---|
| Download the [installer](https://git-scm.com/download/win)<br/><br/>Alternatively you can use the [Chocolatey package manager to install Git](https://chocolatey.org/packages/git). | On 10.9 and above, try running `git --version`. If you don't have git installed already, it will prompt you to install it.<br/><br/>Alternatively, download the [installer](https://git-scm.com/download/mac) | On Linux, you can generally install Git through the distro's package manager.<br/>For RPM-based distros, use<br/>`sudo dnf install git-all`<br/>For Debian-based distros, use<br/>`sudo apt isntall git-all`.|

Quick Git reference ([Everyday Git](https://git-scm.com/docs/giteveryday)).

### Source code

Clone the source code:

```shell
git clone https://gitlab.com/pspc-dsb/myhrapp.git && cd myhrapp
```

The resulting directory structure is:

```shell
► .fb                    # Firebase config files.
► scripts                # Firebase, Hugo, and Workbox scripts for project automation.
▼ src                    # App source code and content (Hugo)
  └► archetypes
  └► assets              # Custom CSS/JS files
  └► content             # Content managed by CMS
     └► en               # English content files
     └► fr               # French content files
  └► data
  └► resources
  └► static              # Static files served at app root (/)
     └► admin            # Netlify CMS addon and config
     └► images           # Web app images
     └─ firebase-messaging-sw.js # Push notification code
     └─ manifest.json    # PWA config file
     └─ sw.js            # Service worker file generated by Workbox
     └─ sw.js.map        # Source map for sw.js
  └► themes              # Theme folders that Hugo merges with the folders above
     └► archetypes
     └► assets
     └► i18n             # Language variable files
     └► layouts          # All the site template files are here
     └► static           # Static files served at app root (/)
  └─ config.yml          # Hugo config file
  └─ workbox-config.js   # Workbox config file
.env.example             # Example of a .env file. Put your Firebase cli authentication token in .env.
.gitignore
.gitlab-ci.yml           # GitLab pipeline file that defines build/test/deploy jobs.
LICENSE
README.md
```

## Usage

### Run the Hugo Dev server:

For most development, **this is the only command you'll need**. It runs runs a local server that listens for file changes, rebuilds as needed, and reloads the browser if you have the site open at http://localhost:8080.

```shell
scripts/hugo-serve-dev
```

### Hugo build:

To see the build artifacts that Hugo produces, you could run `scripts/hugo-build`. Output will be in *.fb/public/*. In general, `scripts/hugo-build` and the `scripts/workbox-update-cache` script aren't needed for development - they're used in the pipeline for UAT/Prod deployments.

### Firebase setup:

1. Create a Firebase project at https://console.firebase.google.com/ (it will use the free Spark plan by default). 
    1. Choose a project name. Note that the project name will determine the "unique identifier", which will be your free subdomain name (.web.app will be added to it).
    1. Leave Analytics enabled.
    1. Configure Analytics, or choose an existing Analytics account.
1. Enable anonymous authentication.
    1. Click the **Authentication** menu item
    1. Click the **Sign-in method** tab
    1. Enable **Anonymous** authentication
1. Enable **Firestore**
    1. Click the **Firestore** menu item
    1. Click the **Create database** button
    1. The rules mode doesn't matter, because .fb/firestore.rules will override this with anonymous access later, so click **Next**
    1. Choose the datacentre region closest to your users, e.g. northamerica-northeast1 (which is in Montreal)
    1. Optionally, edit your rules in the **Rules** tab (copy contents of .fb/firestore.rules into the page)
1. Set up UAT and Prod sites in the **Hosting** menu item
    1. Click **Get started**
    1. Skip step 1 (Install Firebase CLI), because it's dockerized in this project already
    1. Skip step 2 (Initialize your project), because login is handled by a token, and init is handled by the firebase script in this project
    1. Skip step 3 (Deploy to Firebase Hosting), because this is also scripted
    1. Click **Continue to console**
    1. This created a prod site with the same name as your project. If you want another site, you can scroll down, and click the **Add another site** button. E.g. <project-name>-uat
1. Set up Analytics
    1. Click **Dashboard** under Analytics in the left-hand menu
    1. Click the **</>** button to add a web app to Anylitics
    1. Add an app nickname like <project-name>-app. **Don't** select the option to create hosting for it.
    1. Copy the **firebaseConfig** object, and nothing else, into *src/assets/js/firebase/init.js*. The apiKey isn't private - it's just used by your app's users to connect to the right firebase project api.
    1. Click **Continue to console**
    1. Click the settings gear next to **Project Overview**, then **Project settings**
    1. In the "Your apps" section, click the **Link to a Firebase Hosting site** button
    1. Choose your prod site
1. Set up Push Notifications (Firebase Messaging)
    1. Click the settings gear next to **Project Overview**, then **Project settings**
    1. Click the **Cloud Messaging** tab
    1. In the "Web configuration" section, click the **Generate key pair**
    1. Copy the "Key pair" value to *src/assets/js/firebase/messaging.js* to the `messaging.usePublicVapidKey` function.
1. Add firebase values to your .env file (using .env.example as a guide)
    1. To get your value for **FIREBASE_TOKEN**, run `scripts/firebase login:ci`, follow the prompts, and copy the token it outputs.
    1. **FIREBASE_PROJECT** is the project name (lowercase one) from above
    1. **FIREBASE_SITE** is one of your site names from above (use the prod one in GitLab CI variables, and one of the others in your .env file for testing)
    1. **FIREBASE_TARGET** is one of the aliases (test/stage/uat/prod) you want to give to your site
1. Add the first 3 **FIREBASE_** variables above to your GitLab project's CI variables. The target isn't needed there. Make sure to mask the token variable, and to set all of the variables as unprotected. While you're here, add the **HUGO_** variables from .env.example and .env to GitLab CI variables too.

### Firebase scripts:

The `scripts/firebase-serve-dev` script will run a dev server similar to the hugo one (at http://localhost:5000), except it only serves content from *.fb/public*, so you have to run the `scripts/hugo-build` script beforehand. It's not really useful, except for testing that the build picks up everything you expect, and that Firebase will serve it as expected.

The `scripts/firebase-deploy` script grabs what's in `.fb/public` and deploys it to UAT. It can be configured to deploy to Prod too. However, it's best to leave deployment to the pipeline.

Arbitrary Firebase CLI commands can be run using `scripts/firebase`. You'll need this for Firebase setup tasks like getting a command line authentication token to store in GitLab's CI/CD variables, and in your .env file.

### Netlify CMS setup:

The config file is *src/static/admin/config.yml*. You need to customize the **repo** and the **api_id**. The **api_id** is a token you get from GitLab:

1. Go to your GitLab user profile page
1. Click the **Applications** menu item on the left
1. Enter "Netlify-CMS" in the **Name** field
1. Enter "https://<prod-site>.web.app/admin/" in the **Redirect URI** field
1. Uncheck the **Confidential** checkbox
1. Check the **api** checkbox
1. Click the **Save application** button
1. Copy the **Application ID** token it gives you to **api_id** in *src/static/admin/config.yml*

## Contributing

See [Hugo - Getting Started](https://gohugo.io/getting-started/) to learn about Hugo.

See [Get started with Firebase](https://firebase.google.com/docs/web/setup) to learn about Firebase.

See [Get started with Workbox](https://developers.google.com/web/tools/workbox/guides/get-started) to learn about offline service-worker caching with Workbox.

If you'd like to contribute, please fork the repository and make changes as you'd like. Merge requests are warmly welcome.